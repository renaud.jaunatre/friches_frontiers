# Friches_Frontiers

This git repository contains the data and code used for the paper *Soil fertility and landscape surrounding former arable fields drive the ecological resilience of Mediterranean dry grassland plant communities*.

